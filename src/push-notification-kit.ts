import { AppError, ErrorType } from './types'
import { Result, ok, err } from 'neverthrow'
import { Routes, API_VERSION } from './constants'
import { AxiosInstance } from 'axios'
import createHttpClient from './axios'

export default class NotificationKitServer {
  private apiKey: string

  private appId: string

  private apiVersion = API_VERSION

  private _initialized: boolean = false

  private httpClient: AxiosInstance

  private async guard<T>(
    cb: (...args: any[]) => Promise<T | AppError>
  ): Promise<Result<T, AppError>> {
    if (!this._initialized) {
      return err(
        new AppError(
          'NotificationKit has not been initialized!',
          ErrorType.InternalErrorType.Uninitialized
        )
      )
    }
    try {
      const res = await cb()
      if (res instanceof AppError) {
        return err(res)
      }
      return ok(res)
    } catch (e) {
      if (e instanceof AppError) {
        return err(e)
      }
      return err(new AppError('Unknown error', ErrorType.InternalErrorType.Unknown))
    }
  }

  constructor(options: {
    notificationAppId: string
    notificationApiKey: string
    apiVersion?: string
    notificationUrl?: string
  }) {
    const { notificationApiKey, notificationAppId, apiVersion, notificationUrl } = options
    this.apiKey = notificationApiKey
    this.appId = notificationAppId
    this.apiVersion = apiVersion || this.apiVersion
    this.httpClient = createHttpClient({
      baseURL: `${notificationUrl}/${this.apiVersion}/apps/${this.appId}`,
      apiKey: this.apiKey
    })
    this._initialized = true
  }

  public async registerDeviceToken(data: {
    userId: string
    instanceId: string
    token: string
  }): Promise<Result<{ status: number }, AppError>> {
    return this.guard(async () => {
      const {
        Register: { Endpoint, Method }
      } = Routes
      const { status } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data
      })
      return { status }
    })
  }

  public async unregisterDevice(instanceId: string): Promise<Result<{ status: number }, AppError>> {
    return this.guard(async () => {
      const {
        Unregister: { Endpoint, Method }
      } = Routes
      const { status } = await this.httpClient({
        url: Endpoint(instanceId),
        method: Method
      })
      return { status }
    })
  }

  public async pushNotifications(params: {
    title?: string
    body?: string
    userIds: string[]
    data?: { [key: string]: string | number }
    imageUrl?: string
    androidImageUrl?: string
    sound?: string
    iosImageUrl?: string
    channelId?: string
    threadId?: string
    category?: string
    iosTag?: string
    androidTag?: string
  }): Promise<Result<{ status: number }, AppError>> {
    return this.guard(async () => {
      const {
        SendNotification: { Endpoint, Method }
      } = Routes
      const { status } = await this.httpClient({
        url: Endpoint,
        method: Method,
        data: params
      })
      return { status }
    })
  }
}
