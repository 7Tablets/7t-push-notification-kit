import axios, { AxiosRequestConfig, AxiosError } from 'axios'
import { API_KEY_NAME } from './constants'
import { AppError, ErrorType } from './types'

function checkApiKey(config: AxiosRequestConfig) {
  if (!config.headers[API_KEY_NAME]) {
    throw new AppError('Missing api key in request header', ErrorType.InternalErrorType.AuthMissing)
  }
  return config
}

function requestFailed(error: AxiosError) {
  const { response } = error
  const err = new AppError(response?.data.message, response?.data.type)
  err.setStatusCode(response?.status)
  err.setErrorActions(response?.data.actions)
  throw err
}

export default (config: { baseURL: string; apiKey: string }) => {
  const { baseURL, apiKey } = config
  const instance = axios.create({
    baseURL,
    headers: {
      [API_KEY_NAME]: apiKey
    }
  })
  instance.interceptors.request.use(checkApiKey)
  instance.interceptors.response.use(undefined, requestFailed)
  return instance
}
