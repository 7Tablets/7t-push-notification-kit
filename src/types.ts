/**
 * HTTP Methods
 */

export enum HttpMethod {
  Post = 'post',
  Get = 'get',
  Delete = 'delete',
  Patch = 'patch',
  Put = 'put'
}

export namespace ErrorType {
  export enum AuthErrorType {
    InvalidKeyErrorType = 'AuthErrorTypeInvalidKey',
    InvalidUserType = 'AuthErrorTypeInvalidUserType',
    NotFoundErrorType = 'AuthErrorTypeNotFound',
    MissingParamsErrorType = 'AuthErrorTypeMissingParams',
    InvalidPassword = 'AuthErrorTypeInvalidPassword',
    TokenExpiredType = 'AuthErrorTypeTokenExpired',
    NotPartOfApp = 'AuthErrorTypeNotPartOfApp',
    InvalidWebhookKey = 'AuthErrorTypeInvalidWebhookKey',
    NoAccessToUser = 'AuthErrorTypeNoAccessToUser',
    ServerNoAccessToUser = 'AuthErrorTypeServerNoAccessToUser',
    IncorrectPassword = 'AuthErrorTypeIncorrectPassword',
    NoSamePassword = 'AuthErrorTypeNoSamePassword',
    NoAppAccessToUser = 'AuthErrorTypeNoAppAccessToUser',
    TokenNotStarted = 'AuthErrorTypeTokenNotStarted',
    AccountAlreadyExists = 'AuthErrorTypeAccountAlreadyExists',
    UserNotFound = 'AuthErrorTypeUserNotFound',
    InvalidToken = 'AuthErrorTypeInvalidToken',
    MissingAuthHeaderField = 'AuthErrorTypeMissingAuthHeaderField',
    InvalidCredentials = 'AuthErrorTypeInvalidCredentials',
    HashGeneration = 'AuthErrorTypeHashGeneration',
    HashComparison = 'AuthErrorTypeHashComparison',
    RefreshTokenAlreadyExchanged = 'AuthErrorTypeRefreshTokenAlreadyExchanged',
    RefreshTokenNotFound = 'AuthErrorTypeRefreshTokenNotFound'
  }
  export enum ServerErrorType {
    UnknownError = 'UnknownError',
    InternalServerError = 'InternalServerError'
  }
  export enum DeviceTokenErrorType {
    InvalidPushParams = 'DeviceTokenErrorTypeInvalidPushParams',
    AlreadyRegisteredForUser = 'DeviceTokenErrorTypeAlreadyRegisteredForUser',
    NotFound = 'DeviceTokenErrorTypeNotFound',
    NoTokensToPush = 'DeviceTokenErrorTypeNoTokensToPush',
    OnlyStringData = 'DeviceTokenErrorTypeOnlyStringData'
  }

  export enum InternalErrorType {
    Uninitialized = 'InternalUninitialized',
    Unknown = 'InternalUnknown',
    AuthMissing = 'InternalDetailsMissing'
  }

  export type Type = InternalErrorType | DeviceTokenErrorType | ServerErrorType | AuthErrorType
}

export class AppError {
  errorType?: ErrorType.Type
  statusCode?: number
  error?: Error
  message?: string
  authFailed?: boolean
  exchange?: boolean

  setErrorActions = (params: { authFailed?: boolean; exchange?: boolean } = {}) => {
    const { authFailed, exchange } = params
    this.exchange = exchange
    this.authFailed = authFailed
  }

  constructor(err?: string | Error, errorType?: ErrorType.Type) {
    if (errorType) {
      this.errorType = errorType
    }
    if (err instanceof Error) {
      this.error = err
      this.message = err.message
    } else {
      this.message = err
    }
  }

  setMessage = (msg: string) => {
    this.message = msg
  }

  setStatusCode = (statusCode?: number) => {
    this.statusCode = statusCode
  }
}
