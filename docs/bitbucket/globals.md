[@7t/push-notification-kit](README.md) › [Globals](globals.md)

# @7t/push-notification-kit

## Index

### Modules

* [ErrorType](modules/errortype.md)

### Enumerations

* [HttpMethod](enums/httpmethod.md)

### Classes

* [AppError](classes/apperror.md)
* [NotificationKitServer](classes/notificationkitserver.md)

### Variables

* [API_KEY_NAME](globals.md#markdown-header-const-api_key_name)
* [API_VERSION](globals.md#markdown-header-const-api_version)
* [MESSAGE_READ_UPDATE_INTERVAL](globals.md#markdown-header-const-message_read_update_interval)

### Functions

* [checkApiKey](globals.md#markdown-header-checkapikey)
* [requestFailed](globals.md#markdown-header-requestfailed)

### Object literals

* [Routes](globals.md#markdown-header-const-routes)

## Variables

### `Const` API_KEY_NAME

• **API_KEY_NAME**: *"x-notification-server-api-key"* = "x-notification-server-api-key"

Defined in constants.ts:5

___

### `Const` API_VERSION

• **API_VERSION**: *"v1"* = "v1"

Defined in constants.ts:3

___

### `Const` MESSAGE_READ_UPDATE_INTERVAL

• **MESSAGE_READ_UPDATE_INTERVAL**: *5000* = 5000

Defined in constants.ts:7

## Functions

###  checkApiKey

▸ **checkApiKey**(`config`: AxiosRequestConfig): *AxiosRequestConfig*

Defined in axios.ts:5

**Parameters:**

Name | Type |
------ | ------ |
`config` | AxiosRequestConfig |

**Returns:** *AxiosRequestConfig*

___

###  requestFailed

▸ **requestFailed**(`error`: AxiosError): *void*

Defined in axios.ts:12

**Parameters:**

Name | Type |
------ | ------ |
`error` | AxiosError |

**Returns:** *void*

## Object literals

### `Const` Routes

### ▪ **Routes**: *object*

Defined in constants.ts:9

▪ **Register**: *object*

Defined in constants.ts:10

* **Endpoint**: *string* = "/device-tokens"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **SendNotification**: *object*

Defined in constants.ts:18

* **Endpoint**: *string* = "/push"

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Post

▪ **Unregister**: *object*

Defined in constants.ts:14

* **Method**: *[HttpMethod](enums/httpmethod.md)* = HttpMethod.Delete

* **Endpoint**(`instanceId`: string): *string*
