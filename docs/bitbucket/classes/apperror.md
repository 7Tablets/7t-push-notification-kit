[@7t/push-notification-kit](../README.md) › [Globals](../globals.md) › [AppError](apperror.md)

# Class: AppError

## Hierarchy

* **AppError**

## Index

### Constructors

* [constructor](apperror.md#markdown-header-constructor)

### Properties

* [authFailed](apperror.md#markdown-header-optional-authfailed)
* [error](apperror.md#markdown-header-optional-error)
* [errorType](apperror.md#markdown-header-optional-errortype)
* [exchange](apperror.md#markdown-header-optional-exchange)
* [message](apperror.md#markdown-header-optional-message)
* [statusCode](apperror.md#markdown-header-optional-statuscode)

### Methods

* [setErrorActions](apperror.md#markdown-header-seterroractions)
* [setMessage](apperror.md#markdown-header-setmessage)
* [setStatusCode](apperror.md#markdown-header-setstatuscode)

## Constructors

###  constructor

\+ **new AppError**(`err?`: string | Error, `errorType?`: [Type](../modules/errortype.md#markdown-header-type)): *[AppError](apperror.md)*

Defined in types.ts:72

**Parameters:**

Name | Type |
------ | ------ |
`err?` | string &#124; Error |
`errorType?` | [Type](../modules/errortype.md#markdown-header-type) |

**Returns:** *[AppError](apperror.md)*

## Properties

### `Optional` authFailed

• **authFailed**? : *undefined | false | true*

Defined in types.ts:65

___

### `Optional` error

• **error**? : *Error*

Defined in types.ts:63

___

### `Optional` errorType

• **errorType**? : *[Type](../modules/errortype.md#markdown-header-type)*

Defined in types.ts:61

___

### `Optional` exchange

• **exchange**? : *undefined | false | true*

Defined in types.ts:66

___

### `Optional` message

• **message**? : *undefined | string*

Defined in types.ts:64

___

### `Optional` statusCode

• **statusCode**? : *undefined | number*

Defined in types.ts:62

## Methods

###  setErrorActions

▸ **setErrorActions**(`params`: object): *void*

Defined in types.ts:68

**Parameters:**

▪`Default value`  **params**: *object*= {}

Name | Type |
------ | ------ |
`authFailed?` | undefined &#124; false &#124; true |
`exchange?` | undefined &#124; false &#124; true |

**Returns:** *void*

___

###  setMessage

▸ **setMessage**(`msg`: string): *void*

Defined in types.ts:86

**Parameters:**

Name | Type |
------ | ------ |
`msg` | string |

**Returns:** *void*

___

###  setStatusCode

▸ **setStatusCode**(`statusCode?`: undefined | number): *void*

Defined in types.ts:90

**Parameters:**

Name | Type |
------ | ------ |
`statusCode?` | undefined &#124; number |

**Returns:** *void*
