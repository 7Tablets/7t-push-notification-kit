[@7t/push-notification-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [InternalErrorType](errortype.internalerrortype.md)

# Enumeration: InternalErrorType

## Index

### Enumeration members

* [AuthMissing](errortype.internalerrortype.md#markdown-header-authmissing)
* [Uninitialized](errortype.internalerrortype.md#markdown-header-uninitialized)
* [Unknown](errortype.internalerrortype.md#markdown-header-unknown)

## Enumeration members

###  AuthMissing

• **AuthMissing**: = "InternalDetailsMissing"

Defined in types.ts:54

___

###  Uninitialized

• **Uninitialized**: = "InternalUninitialized"

Defined in types.ts:52

___

###  Unknown

• **Unknown**: = "InternalUnknown"

Defined in types.ts:53
