[@7t/push-notification-kit](../README.md) › [Globals](../globals.md) › [HttpMethod](httpmethod.md)

# Enumeration: HttpMethod

HTTP Methods

## Index

### Enumeration members

* [Delete](httpmethod.md#markdown-header-delete)
* [Get](httpmethod.md#markdown-header-get)
* [Patch](httpmethod.md#markdown-header-patch)
* [Post](httpmethod.md#markdown-header-post)
* [Put](httpmethod.md#markdown-header-put)

## Enumeration members

###  Delete

• **Delete**: = "delete"

Defined in types.ts:8

___

###  Get

• **Get**: = "get"

Defined in types.ts:7

___

###  Patch

• **Patch**: = "patch"

Defined in types.ts:9

___

###  Post

• **Post**: = "post"

Defined in types.ts:6

___

###  Put

• **Put**: = "put"

Defined in types.ts:10
