[@7t/push-notification-kit](../README.md) › [Globals](../globals.md) › [ErrorType](../modules/errortype.md) › [DeviceTokenErrorType](errortype.devicetokenerrortype.md)

# Enumeration: DeviceTokenErrorType

## Index

### Enumeration members

* [AlreadyRegisteredForUser](errortype.devicetokenerrortype.md#markdown-header-alreadyregisteredforuser)
* [InvalidPushParams](errortype.devicetokenerrortype.md#markdown-header-invalidpushparams)
* [NoTokensToPush](errortype.devicetokenerrortype.md#markdown-header-notokenstopush)
* [NotFound](errortype.devicetokenerrortype.md#markdown-header-notfound)
* [OnlyStringData](errortype.devicetokenerrortype.md#markdown-header-onlystringdata)

## Enumeration members

###  AlreadyRegisteredForUser

• **AlreadyRegisteredForUser**: = "DeviceTokenErrorTypeAlreadyRegisteredForUser"

Defined in types.ts:45

___

###  InvalidPushParams

• **InvalidPushParams**: = "DeviceTokenErrorTypeInvalidPushParams"

Defined in types.ts:44

___

###  NoTokensToPush

• **NoTokensToPush**: = "DeviceTokenErrorTypeNoTokensToPush"

Defined in types.ts:47

___

###  NotFound

• **NotFound**: = "DeviceTokenErrorTypeNotFound"

Defined in types.ts:46

___

###  OnlyStringData

• **OnlyStringData**: = "DeviceTokenErrorTypeOnlyStringData"

Defined in types.ts:48
