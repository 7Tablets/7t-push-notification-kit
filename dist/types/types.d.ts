/**
 * HTTP Methods
 */
export declare enum HttpMethod {
    Post = "post",
    Get = "get",
    Delete = "delete",
    Patch = "patch",
    Put = "put"
}
export declare namespace ErrorType {
    enum AuthErrorType {
        InvalidKeyErrorType = "AuthErrorTypeInvalidKey",
        InvalidUserType = "AuthErrorTypeInvalidUserType",
        NotFoundErrorType = "AuthErrorTypeNotFound",
        MissingParamsErrorType = "AuthErrorTypeMissingParams",
        InvalidPassword = "AuthErrorTypeInvalidPassword",
        TokenExpiredType = "AuthErrorTypeTokenExpired",
        NotPartOfApp = "AuthErrorTypeNotPartOfApp",
        InvalidWebhookKey = "AuthErrorTypeInvalidWebhookKey",
        NoAccessToUser = "AuthErrorTypeNoAccessToUser",
        ServerNoAccessToUser = "AuthErrorTypeServerNoAccessToUser",
        IncorrectPassword = "AuthErrorTypeIncorrectPassword",
        NoSamePassword = "AuthErrorTypeNoSamePassword",
        NoAppAccessToUser = "AuthErrorTypeNoAppAccessToUser",
        TokenNotStarted = "AuthErrorTypeTokenNotStarted",
        AccountAlreadyExists = "AuthErrorTypeAccountAlreadyExists",
        UserNotFound = "AuthErrorTypeUserNotFound",
        InvalidToken = "AuthErrorTypeInvalidToken",
        MissingAuthHeaderField = "AuthErrorTypeMissingAuthHeaderField",
        InvalidCredentials = "AuthErrorTypeInvalidCredentials",
        HashGeneration = "AuthErrorTypeHashGeneration",
        HashComparison = "AuthErrorTypeHashComparison",
        RefreshTokenAlreadyExchanged = "AuthErrorTypeRefreshTokenAlreadyExchanged",
        RefreshTokenNotFound = "AuthErrorTypeRefreshTokenNotFound"
    }
    enum ServerErrorType {
        UnknownError = "UnknownError",
        InternalServerError = "InternalServerError"
    }
    enum DeviceTokenErrorType {
        InvalidPushParams = "DeviceTokenErrorTypeInvalidPushParams",
        AlreadyRegisteredForUser = "DeviceTokenErrorTypeAlreadyRegisteredForUser",
        NotFound = "DeviceTokenErrorTypeNotFound",
        NoTokensToPush = "DeviceTokenErrorTypeNoTokensToPush",
        OnlyStringData = "DeviceTokenErrorTypeOnlyStringData"
    }
    enum InternalErrorType {
        Uninitialized = "InternalUninitialized",
        Unknown = "InternalUnknown",
        AuthMissing = "InternalDetailsMissing"
    }
    type Type = InternalErrorType | DeviceTokenErrorType | ServerErrorType | AuthErrorType;
}
export declare class AppError {
    errorType?: ErrorType.Type;
    statusCode?: number;
    error?: Error;
    message?: string;
    authFailed?: boolean;
    exchange?: boolean;
    setErrorActions: (params?: {
        authFailed?: boolean | undefined;
        exchange?: boolean | undefined;
    }) => void;
    constructor(err?: string | Error, errorType?: ErrorType.Type);
    setMessage: (msg: string) => void;
    setStatusCode: (statusCode?: number | undefined) => void;
}
