import { HttpMethod } from './types';
export declare const API_VERSION = "v1";
export declare const API_KEY_NAME = "x-notification-server-api-key";
export declare const MESSAGE_READ_UPDATE_INTERVAL = 5000;
export declare const Routes: {
    Register: {
        Endpoint: string;
        Method: HttpMethod;
    };
    Unregister: {
        Endpoint: (instanceId: string) => string;
        Method: HttpMethod;
    };
    SendNotification: {
        Endpoint: string;
        Method: HttpMethod;
    };
};
