"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var constants_1 = require("./constants");
var types_1 = require("./types");
function checkApiKey(config) {
    if (!config.headers[constants_1.API_KEY_NAME]) {
        throw new types_1.AppError('Missing api key in request header', types_1.ErrorType.InternalErrorType.AuthMissing);
    }
    return config;
}
function requestFailed(error) {
    var response = error.response;
    var err = new types_1.AppError(response === null || response === void 0 ? void 0 : response.data.message, response === null || response === void 0 ? void 0 : response.data.type);
    err.setStatusCode(response === null || response === void 0 ? void 0 : response.status);
    err.setErrorActions(response === null || response === void 0 ? void 0 : response.data.actions);
    throw err;
}
exports.default = (function (config) {
    var _a;
    var baseURL = config.baseURL, apiKey = config.apiKey;
    var instance = axios_1.default.create({
        baseURL: baseURL,
        headers: (_a = {},
            _a[constants_1.API_KEY_NAME] = apiKey,
            _a)
    });
    instance.interceptors.request.use(checkApiKey);
    instance.interceptors.response.use(undefined, requestFailed);
    return instance;
});
//# sourceMappingURL=axios.js.map