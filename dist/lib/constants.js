"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = exports.MESSAGE_READ_UPDATE_INTERVAL = exports.API_KEY_NAME = exports.API_VERSION = void 0;
var types_1 = require("./types");
exports.API_VERSION = 'v1';
exports.API_KEY_NAME = 'x-notification-server-api-key';
exports.MESSAGE_READ_UPDATE_INTERVAL = 5000;
exports.Routes = {
    Register: {
        Endpoint: '/device-tokens',
        Method: types_1.HttpMethod.Post
    },
    Unregister: {
        Endpoint: function (instanceId) { return "/device-tokens/" + instanceId; },
        Method: types_1.HttpMethod.Delete
    },
    SendNotification: {
        Endpoint: '/push',
        Method: types_1.HttpMethod.Post
    }
};
//# sourceMappingURL=constants.js.map